package com.jsf.springbootjsf.repositorio

import groovy.transform.ToString

@ToString
class Contato {
    Long id
    String nome
    String email
    String mensagem
    String sexo
}