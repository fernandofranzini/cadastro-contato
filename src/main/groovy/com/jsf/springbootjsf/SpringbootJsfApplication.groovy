package com.jsf.springbootjsf

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.web.servlet.ServletRegistrationBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Profile

import javax.faces.webapp.FacesServlet
import javax.servlet.ServletContext

@SpringBootApplication
class SpringbootJsfApplication {

    static void main(String[] args) {
        SpringApplication.run(SpringbootJsfApplication, args)
    }

    // Configuração para usar JSF Pedro Casagrande
    @Bean
    @Profile("prod")
    ServletRegistrationBean jsfServletRegistration(ServletContext servletContext) {
        servletContext.setInitParameter("com.sun.faces.forceLoadConfiguration", Boolean.TRUE.toString())
        ServletRegistrationBean srb = new ServletRegistrationBean()
        srb.setServlet(new FacesServlet())
        srb.setUrlMappings(Arrays.asList("*.faces"))
        srb.setLoadOnStartup(1)
        return srb
    }
}
