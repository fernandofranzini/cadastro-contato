package com.jsf.springbootjsf.form

import com.jsf.springbootjsf.repositorio.Contato
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.springframework.stereotype.Component

import javax.annotation.PostConstruct

@Component
class HelloFrm {

    private static final Logger LOG = LogManager.getLogger(HelloFrm.class)

    Contato contato = new Contato()
    List<Contato> lista = []

    @PostConstruct
    void pesquisar() {
        3.times {
            lista << new Contato(id: 1, nome: "Fer${it}", email: "email${it}.gmail.com", mensagem: "Vai curintia")
        }
    }

    void gravar() {
        LOG.info("gravando novo contato ${contato}")
        lista << contato
        contato = new Contato()
        LOG.info("FOi alterado alterado - 1")
    }
}
